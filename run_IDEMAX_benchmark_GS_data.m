close all hidden; close all; clear all; clc
addpath(genpath('/afs/pdc.kth.se/home/s/secilmis/GS'))
addpath(genpath('/afs/pdc.kth.se/home/s/secilmis/GS/genespider'))
addpath(genpath('/afs/pdc.kth.se/home/s/secilmis/SubsetSelection'))

cd ~/IDEMAX/Datasets/GS

halfpath = "~/IDEMAX/Datasets/GS";
sizes = [100,250,500];
set = [1,2,3,4,5];


methods = {'lsco', 'Glmnet', 'RidgeCO'};

%%%% GRN inference using Intended perturbation
for kk = 1:length(sizes)
  for i = 1:length(set)
    for j = 1:length(methods)
      load(strcat('~/IDEMAX/Datasets/GS/N', num2str(sizes(kk)), '/dataset_scaleFreeNet', '_N', num2str(sizes(kk)), '_SNR_0.001.mat'))
      Aest = Methods.(methods{j})(data(i), logspace(-6,0,20));
      M = analyse.CompareModels(net(i), Aest);
      cd ~/IDEMAX/GS/matFiles/IntendedP
      save(fullfile([methods{j}, '_HigherVar_', num2str(data(i).N), '_', num2str(i), '.mat']), 'Aest', 'M')
      cd ~/IDEMAX/GS
      dlmwrite(fullfile(['GS_HigherVar_IntendedP','.csv']),(vertcat(data(i).N, i, j, M.AUROC, M.AUPR))','-append');
    end
  end
end

%%%% GRN inference using random column perturbation
for kk = 1:length(sizes)
  for i = 1:length(set)
    load(strcat('~/IDEMAX/Datasets/GS/N', num2str(sizes(kk)), '/dataset_scaleFreeNet', '_N', num2str(sizes(kk)), '_SNR_0.001.mat'))
    col = randperm(size(data(i).Y,2));
    for j = 1:length(methods)
      load(strcat('~/IDEMAX/Datasets/GS/N', num2str(sizes(kk)), '/dataset_scaleFreeNet', '_N', num2str(sizes(kk)), '_SNR_0.001.mat'))
      data(i).Y = data(i).Y(:,col);
      Aest = Methods.(methods{j})(data(i), logspace(-6,0,20));
      M = analyse.CompareModels(net(i), Aest);
      cd ~/IDEMAX/GS/matFiles/RandomP
      save(fullfile([methods{j}, '_HigherVar_', num2str(data(i).N), '_', num2str(i), '.mat']), 'Aest', 'M')
      cd ~/IDEMAX/GS
      dlmwrite(fullfile(['GS_HigherVar_RandomP','.csv']),(vertcat(data(i).N, i, j, M.AUROC, M.AUPR))','-append');
    end
  end
end


%%%% GRN inference using inferred perturbation
for kk = 1:length(sizes)
  for i = 1:length(set)
    for j = 1:length(methods)
      load(strcat('~/IDEMAX/Datasets/GS/N', num2str(sizes(kk)), '/dataset_scaleFreeNet', '_N', num2str(sizes(kk)), '_SNR_0.001.mat'))
      dataUpd = idemax(data(i).Y); %this replaces the Intended P with the inferred P
      Pold = data(i).P;
      %% Calculate the P inference accuracy
      final = repmat(0, data(i).N, 2);
      for c1 = 1:size(data(i).P,1)
        aa = find(dataUpd.P(c1,:)~=0);
        bb = find(Pold(c1,:)~=0);
        TP = nnz(Pold(c1,aa));
        FP = length(aa) - TP;
        FN = length(aa) - TP;
        final(c1,1) = TP;
        final(c1,2) = FP; %% FP and FN are the same
      end
      F1 = sum(final(:,1))/(sum(final(:,1))+(sum(final(:,2))+sum(final(:,2)))/2);

      Aest = Methods.(methods{j})(dataUpd, logspace(-6,0,20));
      M = analyse.CompareModels(net(i), Aest);
      cd ~/IDEMAX/GS/matFiles/InferredP
      save(fullfile([methods{j}, '_HigherVar_', num2str(data(i).N), '_', num2str(i), '.mat']), 'Aest', 'M')
      cd ~/IDEMAX/GS
      dlmwrite(fullfile(['GS_HigherVar_InferredP','.csv']),(vertcat(data(i).N, i, j, M.AUROC, M.AUPR, F1))','-append');
      dlmwrite(fullfile(['GS_HigherVar_P_accuracy', num2str(data(i).N), '_', num2str(i), '.csv']),final);
    end
  end
end
