"""
IDEMAX - Infer DEsign MAtriX
by Deniz Seçilmiş, Thomas Hillerton, Sven Nelander  and Erik L. L. Sonnhammer

Department of Biochemistry and Biophysics, Stockholm University,
Science for Life Laboratory, Box 1031, 17121 Solna, Sweden,

Contact:
deniz.secilmis@scilifelab.se
thomas.hillerton@scilifelab.se
erik.sonnhammer@scilifelab.se

It can either be imported in to another python project or ran directly. 
If imported it takes a numpy or pandas array of expression values and a int for replicates
    InferP(data,list_with_NR_replicates)
 
If used directly it takes a csv file with fold change expression values a csv file with a single line with the number of replicates for each gene and a file name to save the predicted perturbation to (will save as csv). 
An optional fourth argument can be given to skip header rows in the file
    python InferP.py datafile.csv replicates.csv pred_perturbations.csv {1}

Note that no matter how it is ran the data should be in fold change. 
"""

import numpy as np
import pandas as pd
import sys


def idemax(data,reps):
    """
    The main function of InferP. takes a input dataset of gene epxression and number of expected replicates 
    and returns a predicted perturbation design based on z-scores
    """

    # check if the input is pandas datafram
    # and if it is change it to numpy array
    if isinstance(data, pd.DataFrame):
        data=data.values

    Ptmp = np.zeros(data.shape)
    P = np.zeros(data.shape)
    
    for row in range(data.shape[0]):
        for col in range(data.shape[1]):
            # calclate z-scores over all values but the current column
            Ptmp[row,col] = (data[row,col]-np.mean(np.delete(data[row,:], col)))/np.std(np.delete(data[row,:], col))

        # get the replicates for this row
        rep = reps[row]
        # make the replicate negative to work with the numpy sorting 
        if rep > 0:
            rep = rep*-1

        # get the top absolute z-values
        ind = np.argpartition(np.abs(Ptmp[row,:]), rep)[rep:]
        # and make a binary perturbation in the corresponding column of P
        P[row,ind] = np.sign(Ptmp[row,ind])
    #finall return ther perturbation in P 
    return P


# define how to use the script if it is called from the command line 
if __name__ == "__main__":
    # parse the input
    expression = sys.argv[1]
    reps = sys.argv[2]
    outfile = sys.argv[3]

    # check if any rows of the input data should be skiped
    if len(sys.argv) > 4:
        skiprow = sys.argv[4]
    else:
        skiprow = None

    #load the data
    data = pd.read_csv(expression, header=None,skiprows=skiprow)

    # read replicates
    reps = pd.read_csv(reps, header=None,skiprows=skiprow)
        
    # then get the predicted P from this 
    Pred_P = idemax(data,reps)

    # finally save the prediction to a csv
    # we do not save the automatic header or index form pandas 
    pd.DataFrame(Pred_P).to_csv(outfile, header=None, index=None)
