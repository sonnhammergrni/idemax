function varargout = idemax(data, Reps)
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  % IDEMAX - Infer DEsign MAtriX
  % by Deniz Seçilmiş, Thomas Hillerton, Sven Nelander  and Erik L. L. Sonnhammer
  %
  % Department of Biochemistry and Biophysics, Stockholm University,
  % Science for Life Laboratory, Box 1031, 17121 Solna, Sweden,
  %
  % Contact:
  % deniz.secilmis@scilifelab.se
  % thomas.hillerton@scilifelab.se
  % erik.sonnhammer@scilifelab.se
  %
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %
  %%% input arguments
  %
  % input arguments can either be variables or paths to data files (csv). In case of variables, the function will output a matrix
  % in case of paths, the function will write a csv file in the current working directory
  %
  %% data: fold change gene expression matrix where rows are the genes (N) and columns are the experiments (M)
  % gene-by-experiment (N x M) foldchange matrix should be an array.
  %
  % reps: a single vector of size gene-by-1 (N x 1) should include number of replicates per gene
  % An example input set and run is given below:
  %
  %
  % data = [1,2,3,2,5,1; 2,8,1,5,2,7]; % 2-by-6 meaning 2 genes and 6 experiments
  % Reps = [3;3]; % 2-by-1 meaning 2 genes, each having 3 replicates
  %% Alternative Reps = [2;4]; meaning gene1 has 2 replicates and gene2 has 4 replicates
  %
  % P_idemax = idemax(data, Reps); %% This will output the inferred perturbation matrix
  % Row-P_idemax: genes
  % Col-P_idemax: experiments
  % Same size as the foldchange expression matrix
  %
  %
  %%% Note that it is important to correctly input Reps
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



  if isa(data,'char')
    fc = readtable(data, 'Delimiter', ',', 'ReadVariableNames', 0);
    foldchange = table2array(fc);
  elseif isa(data, 'double')
      foldchange = data;
  end

  if isa(Reps,'char')
    nrReps = readtable(Reps, 'Delimiter', ',', 'ReadVariableNames', 0);
    nrReps = table2array(nrReps);
  elseif isa(Reps, 'double')
      nrReps = nrReps;
  end



  %% Run
  %% create an empty matrix that will have the further calculated Z-scores
  Ptmp = zeros(size(foldchange));
  %% create another empty matrix - Inferred P to be
  P = zeros(size(foldchange));
  %% Here ind is a vector: ind = 1, 2, ..., M, where M is the number of experiments
  %% For each experiment of, we will use this vector to extract the indices of all the others (for ith experiment, this will be !i)
  ind = 1:size(Ptmp,2);
  for c1 = 1:size(Ptmp,1) %% for each gene
    for c2 = 1:size(Ptmp,2) %% for each experiment
      other = find(ind ~= c2); %% find all experiments but c2 (!c2)
      %% Calculate Z-scores
      Ptmp(c1,c2) = (foldchange(c1,c2) - mean(foldchange(c1,other)))/std(foldchange(c1,other));
    end
    %% Sort absolute Z-scores
    [val, loc] = sort(abs(Ptmp(c1,:)), 'descend');
    %% Find this column in the P matrix and put the sign of the highes absolute Z-score as the inferred perturbation
    P(c1, loc(1:nrReps(c1))) = sign(Ptmp(c1, loc(1:nrReps(c1))));
  end

  if isa(data,'char')
    csvwrite('IDEMAX_P.csv', P)
  else
      varargout{1} = P;
  end
end
