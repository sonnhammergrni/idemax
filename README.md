# IDEMAX

This repository contains all the files relevant to the development and publication of the tool IDEMAX.
There are three key files here that  allow you to run the described tool and predict the perturbation design of a set of gene perturbation experiments, the tool can be ran in MATLAB or Python, see each file for more details about each specific language.

## Idea of IDEMAX
The general idea of the IDEMAX tool is that noise or off-target effects can in some cases mask the underlying gene regulatory network of a gene dataset. To compensate for this we present a tool using Z-score that can find a better fitting perturbation design and thus increase the accuracy of gene regulatory network inference. For more details see the published paper here:

% add paper link here once it is available online

![Figure](https://bitbucket.org/sonnhammergrni/idemax/raw/93bac81bf61503b40b2b9618430bfe3e94b2d5b0/Images/Fig1thinner.png)

A figure showing the idea behind and workflow of the IDEMAX method.

We provide two ways to use the tool:


* idemax.m - for running this algorithm in MATLAB


* idemax.py - for running this algorithm in python (either as import or from terminal)

## Additional files and folders
__Data generation__
The data for this project was generated using the following scripts.
The script GNW_data_generation_script.bash was used to generate the gene netweaver data and the read_GNW_data_to_matlab.m were used to read it in to matlab.
The script generate_GS_data.m was used to generated the GenesSPIDER data.

__Benchmark__
The script for benchmarking IDEMAX for GRN inference is called run_IDEMAX_benchmark_GS_data.m.

__images__
Contains the one image shown above in the document. 
