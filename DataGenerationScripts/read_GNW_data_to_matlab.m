%close all hidden; close all; clear all; clc
%addpath(genpath('~secilmis/GS'))
%addpath(genpath('~secilmis/GS/genespider'))
%addpath(genpath('~secilmis/SubsetSelection'))

% not that the script assums that path contains:
% prefix-1_knockdowns.tsv
% prefix-1_noexpnoise_knockdowns.tsv
% prefix-1_wildtype.tsv
% prefix-1_goldstandard_signed.tsv

% define a set of variables to load the data
halfpath = "~secilmis/Benchmark/Datasets/fullFiles/N"

sizes = [100,250,500];
sets = [1,2,3,4,5];
adjust_val = .6;

for kk = 1:length(sizes)
  for mm =1:length(sets)

      % specify the files to use 
     prefix = strcat('ecoli_transcriptional_network_regulonDB_6_7-1');
     path = strcat(halfpath, num2str(sizes(kk)), '/dataset')
          
    
    % read all the data files in to matlab and change the table in
    % to a matrix
    % starting with the noisy data 
    R1 = readtable(strcat(path,"/rep1/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1);
    names = R1.Properties.VariableNames'; R1 = table2array(R1);
    R2 = readtable(strcat(path,"/rep2/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); R2 = table2array(R2);
    R3 = readtable(strcat(path,"/rep3/",prefix,"_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); R3 = table2array(R3);
    wt1 = readtable(strcat(path,"/rep1/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt1 = table2array(wt1);
    wt2 = readtable(strcat(path,"/rep2/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt2 = table2array(wt2);
    wt3 = readtable(strcat(path,"/rep3/",prefix,"_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); wt3 = table2array(wt3);

    % Then the noise free data
    N1 = readtable(strcat(path,"/rep1/",prefix,"_noexpnoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N1 = table2array(N1);
    N2 = readtable(strcat(path,"/rep2/",prefix,"_noexpnoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N2 = table2array(N2);
    N3 = readtable(strcat(path,"/rep3/",prefix,"_noexpnoise_knockdowns.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); N3 = table2array(N3);

    NW1 = readtable(strcat(path,"/rep1/",prefix,"_noexpnoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW1 = table2array(NW1);
    NW2 = readtable(strcat(path,"/rep2/",prefix,"_noexpnoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW2 = table2array(NW2);
    NW3 = readtable(strcat(path,"/rep3/",prefix,"_noexpnoise_wildtype.csv"), 'Delimiter', '\t', 'ReadVariableNames', 1); NW3 = table2array(NW3);

    % merge the 3 replicates of noisy data 
    data_mat=[R1', R2', R3'];

    % merge the 3 noise free replicates 
    true_data = [N1', N2', N3'];

    % get the total amount of added noise 
    noise_data = data_mat-true_data;

    % we adjust the noise in any position where the total sum of
    % noise and signal becomes 0 or smaller 
    newE = zeros(size(data_mat));
    for i = 1:size(data_mat,1)
        for j = 1:size(data_mat,2)
            
            true_val = true_data(i,j);
            noise_val = noise_data(i,j);

            if true_val+noise_val <= 0
                % noise is adjusted by taking the adjust_val
                % percentage of the signal as the noise 
                newE(i,j) = -true_val*adjust_val;
            else
                newE(i,j) = noise_val;
            end
            
        end
    end
    % once we have the new noise we add this to the signal
    % generating something very close to the original data_mat variable but
    % with no  0s 
    data_mat = true_data+newE;

    % we then do this same thing for the wild type expression  
    wt = [wt1;wt2;wt3];
    wtN = [NW1;NW2;NW3];

    wtE = wt - wtN;

    % as we have 1 wild type for each replicate we take the mean of
    % these to get a more stable value 
    wtN = mean(wtN);
    wtE = mean(wtE);

    % and adjsut the noise for this so we have no 0s in the wild
    % type 
    for i = 1:length(wtE)
        if abs(wtE(i)) >= wtN(i)
            wtE(i) = -wtN(i)*adjust_val;
        else
            wtE(i) = wtE(i); 
        end
    end
   
    wt = wtN+wtE; 

    % once we have this we deffine the size of the data in N and
    % the P matrix as 3Xeye(N)
    Y = data_mat;
    N = size(data_mat, 1);
    P = [-eye(N),-eye(N),-eye(N)];
    
    % once we have adjusted both wild type and experimental
    % expression data we calculate the fold change 
    FCY = zeros(size(Y));
    for i = 1:size(Y,2)
      FCY(:,i) = log2(Y(:,i)./wt');
    end
    Y = FCY;

    % we then do the exact same thing with the noise free data
    % this so that we can later aproximate the noise in the fold
    % change 
    Noise = true_data;

    wtN = [NW1;NW2;NW3];
    wtN = mean(wtN); 
    FCNoise = zeros(size(Noise));
    for i = 1:size(Y,2)
      FCNoise(:,i) = log2(Noise(:,i)./wtN');
    end

    Noise=FCNoise;

    % finally we calculate the noise in the fold change by
    % comparing the noisy fold change with the noise free 
    E = Y - Noise;

    % make a genespider dataset from the generated data
    stdY = std((Y),0,2);
    Yvar = stdY.^2;
    reps=sum(P~=0,2);
    lambda = sum((reps'-1)*Yvar(:)/((reps-1)*length(Yvar(:))),'omitnan');
    lambdas = (Yvar');%./(size(Yvar,2));
    F = zeros(size(P));

    % add all the parameters to a datastructure 
    D.E = E;
    D.F = F;
    D.Y = Y;
    D.P = P;
    D.lambda = [lambda, 0];
    D.sdY = repmat(stdY,1,length(Y));
    D.sdP = zeros(size(D.sdY));
    D.cvY = diag(lambdas);
    D.cvP = zeros(size(D.cvY));

    % and make a dataset 
    data(mm) = datastruct.Dataset(D);

    % then read in the network in to a geneSPIDER network structure 
    fid = fopen(strcat(path,"/rep1/",prefix,"_wildtype.csv"));
    tline = fgetl(fid);
    names = string(split(tline));
    fclose(fid);
    
    Network = read_net(strcat(path,"/rep1/",prefix,'_goldstandard_signed.csv'),names);
    net(mm) = datastruct.Network(Network);
    net(mm).names = names;  

    noise_free_FC(mm).noise = Noise;
  end
  % finally we save this as a mat file that can be loaded in to
  % matlab 
  save_name = strcat("GNW_data_0025_stdvar_noise_adjusted_via_signal_times_",num2str(adjust_val),"_size_",num2str(sizes(kk)),".mat")
  save(save_name, "data","net","noise_free_FC")

end


function net = read_net(path,genes)
% function for reading a cytoscape style tsv network in to a full
% matlab matrix format

    % open the network file
    fid = fopen(path);
    [Mdata,count] = fread(fid,Inf);
    fclose(fid);

    % specify how we want to seperate the lines of the file 
    tab = sprintf('\t');
    lf = sprintf('\n');
    Mdata = [Mdata; lf];
    delimiter = tab;
    Mdata = char(Mdata(:)');
    matchexpr = {'\r\n' '\r' '([ \n])\1+' ' *(\n|\t) *' '^\n'};
    replexpr = {'\n' '\n' '$1' '$1' ''};
    Mdata = regexprep(Mdata,matchexpr,replexpr);
    newlines = find(Mdata == lf); % where is the newline characters
    nlines = length(newlines);

    % select the first line 
    line = Mdata(1:newlines(1)-1);
    line = strsplit(line,delimiter);

    % make a empty network matrix 
    tmpA = zeros(length(genes),length(genes),length(line(3:end)));
    % find the row and column for the gene in the same order as in
    % the expression data (Y)  
    ind1 = find(strcmp(genes,line{1}));
    ind2 = find(strcmp(genes,line{2}));
    % and assign a signed 1 to show there is a link
    for j=1:length(line(3:end))
        if line{3} == "-"
            tmpA(ind1,ind2,j) = 1;
        else
            tmpA(ind1,ind2,j) = -1;
        end
    end

    % after we do this for the first line we loop over all remaning
    % lines and do the same thing
    for i=1:nlines-1
        line = Mdata(newlines(i)+1:newlines(i+1)-1);
        line = strsplit(line,delimiter);
        ind1 = find(strcmp(genes,line{1}));
        ind2 = find(strcmp(genes,line{2}));
        for j=1:length(line(3:end))
            if line{3} == "-"
                tmpA(ind1,ind2,j) = 1;
            else
                tmpA(ind1,ind2,j) = -1;
            end
        end
    end
    % finally we return the matrix as the net variable
    % not that tmpA is transposed as GNW generates networks in
    % row>column regulatory patterns and GS works with column>row 
    net = tmpA';
end

