#!/bin/env bash

# small script that makes a number of GNW datasets each with a selected number of replicates
# it also allows for modification of most parameters without changes to the code to make it
# easier to use.
# not that to run script gnw should be in a folder in pwd such that
# pwd
#    --settings.txt
#    --gnw
#         --gnw.jar
#         --sandbox (folder for default nets)

# not that to change settings for the data generation you will have to alter those in the settings.txt file
# this due to the stand alone version being somewhat feature incomplete for settings options
# the default settings provided with this script is single knockdown pertubations with DREAM4 like noise
# it uses both SDE and ODE model by default and adds self loops to the data 

#### set parameters for number of nets and replicates ####
# total number of uniq networks to make data from 
datasets=5
# total number of replicates to make for each network 
reps=3 

# set the output path. Files will be generated as dataset1. dataset1/rep1 and so on for all reps and datasets
outpath="2020_10_22_GNW_for_benchmark/ODE/all_TFs_N"

#### network generation parameters ####
# input network. If you want to use a GNW default add "Yeast" or "Ecoli"
# else add the path to your tsv file eg data/networks/thomas_network.tsv 
inpnet="Ecoli"
# selection option can be "--greedy-selection" or "--rat-selection number"
select="--greedy-selection" #"--rat-selection 20"
# subnet size, how large should the selected network be
subsize=(100 150 200)
# set minimum requiered regulatory genes. Not that if you select a value larger than the total in the network
# only the maximum possilbe number will be selected
#minreg=50

# De-option, to ease use for people who wishes to load the data to R this option will rename all .tsv output to .csv. Set to 1 for true or 0 for false
DEoption=true

if [ $inpnet="Eoli" ]; then
    inpnet="gnw/sandbox/ecoli_transcriptional_network_regulonDB_6_7.tsv"
fi
if [ $inpnet == "Yeast" ]; then
    inpnet="gnw/sandbox/InSilicoSize10-Yeast1.xml"
fi

for d in "${subsize[@]}"
do
	 savepath=$outpath$d
	 
	 # make sure the outpath exists. Note that it will make directories and parents as needed

	 if [ ! -d $savepath ]; then
	     mkdir -p $savepath
	 fi

	 minreg=(($d/2))
	 
	 # finally before we start make a tmp dir to save networks in temporary
	 mkdir -p "./tmp"

	 for i in $(seq 1 $datasets);
	 do
	     # for each dataset draw a single network
	     java -jar gnw/gnw-3.1.2b.jar --extract -c settings.txt --random-seed --input-net=$inpnet $select --subnet-size=$d --num-subnets=1 --min-num-regulators=$minreg --output-net-format=4 --keep-self-interactions --output-path "./tmp" 

	     curwd=$savepath/dataset$i
	     mkdir -p $curwd
	     worknet=./tmp/*.xml

	     # then make reps number of replicates for each network 
	     for j in $(seq 1 $reps);
	     do
		 # make a rep directory
		 repdir=$curwd/rep$j

		 mkdir -p $repdir
	
		 java -jar gnw/gnw-3.1.2b.jar  --simulate -c settings.txt --input-net $worknet  --output-path $repdir/

		 # move all tsv files as the --output-path option seems to always default to cwd regardless on input
		 mv *.tsv $repdir/
	
	
		 # if using the DEoption now change all tsv to csv
		 # Rename all *.txt to *.text
		 if [ $DEoption = true ]; then
		     for f in $repdir/*.tsv; do
			 mv -- "$f" "${f%.tsv}.csv"
		     done
		 fi
	     done
	     
	     # remove the network from tmp to make a new
	     rm ./tmp/*
	 done
done

# finally remove the empty tmp directory
rm -r ./tmp
echo "All done! Note that the replicates are not normalised!"
