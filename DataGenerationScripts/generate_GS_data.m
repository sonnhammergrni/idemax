% script for generating GeneSPIDER data for InferP paper benchmark 

% specify some general variables 
tol = 1e-4;
IAA = 'low';
SNRs = [0.001,0.01];
Ns = [100,250,500];
n_data_sets = 5;
cpus = 5;

% select where to save the data 
outdir = "/scratch/thomhill/genespider_work/subset_selection_time";
prefix = "dataset_scaleFreeNet";

% option to control if data is saved or just sent to the running
% matlab instance 
dosave=true;

% make the out directory if it is missing 
if ~exist(outdir,'dir')
    mkdir(outdir);
end

% set the probabilities for removing links from the symetrical
% network in cutsym 
pout = 0.398;
pin=0.6;

for g = 1:length(Ns)

    % select the size and number of average edges 
    N=Ns(g);
    S = 3/N;

    % make the networks
    % this is done first and in parallel as it takes a lot of time
    % compared to the data generation 
    disp("making nets")
    parfor (j = 1:n_data_sets,cpus)        
        A(:,:,j) = zeros(N);
        counter(j) = 0;
        % make a while loop that forces the networks to fulfill criterias we want
        while cond(A(:,:,j)) > 1000 | rank(A(:,:,j)) < N
            % make a random stable network 
            A(:,:,j) = datastruct.scalefree(N, S, randn(N)<S);
            A(:,:,j) = datastruct.cutSym(A(:,:,j), pin, pout);
            A(:,:,j) = datastruct.stabilize(A(:,:,j),'iaa',IAA);
            counter(j) = counter(j)+1
            if counter(j) > 1000
                disp(strcat("Failed to find A with cond and rank as specified at,",num2str(j)))
                break
            end
            
        end

        % save all networks in a single loopable variable 
        allnet(j) = datastruct.Network(A(:,:,j),'scalefree');
            
    end

    % once all networks are done we make the actual data 
    for j = 1:n_data_sets
        % first we select which true network to use 
        net = allnet(j); 

        % and generate a shuffled P matrix.
        % the shuffling is to make it slightly harder for methods
        % using inverse of Y to find A under low noise 
        P = double([-eye(N),-eye(N),-eye(N)]); 
        p_shuf = randperm(N*3);
        P = P(:,p_shuf);

        % from this true network and pertubation we then generate
        % all the data we want with the varying SNR levels 
        for i = 1:length(SNRs)
                
            SNR = SNRs(i);
            SNR
                
            Y = net.G*P;

            stdY = std((Y),0,2);
            Yvar = stdY.^2;
            reps=sum(P~=0,2);
            lambda = sum((reps'-1)*Yvar(:)/((reps-1)*length(Yvar(:))),'omitnan');

            s = svd(Y);
            stdE = s(N)/(SNR*sqrt(chi2inv(1-analyse.Data.alpha,prod(size(P)))));

            E = stdE*randn(size(P));

            F = zeros(size(P));

            D.network = net.network;
            D.E = E;
            D.F = F;
            D.Y = Y+E;
            D.P = P;
            D.lambda =[stdE^2,0];
            D.cvY = D.lambda(1)*eye(N);
            D.cvP = zeros(N);
            D.sdY = stdE*ones(size(D.P));
            D.sdP = zeros(size(D.P));
            D.A=net.A;
            
            % collect the data if it is needed later on 
            data = datastruct.Dataset(D,net);

            % here we save the data if this option is turned on
            % if not it will simply be returned to the running
            % matlab instance 
            if dosave
                dir_path = strcat("/N"+num2str(N)+"/");
                full_path = strcat(outdir,dir_path)

           
                if ~exist(full_path,'dir')
                    mkdir(full_path);
                end
            
                name = strcat(prefix+num2str(j)+"_N"+num2str(N)+"_SNR_"+num2str(SNR)+".mat");
                fullname = fullfile(full_path,name);
                save(fullname, 'net', 'data')
                clear a aa A;
            end
        end
    end
end